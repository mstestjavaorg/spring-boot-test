package com.demo.ordermgmnt.controller;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.ordermgmnt.model.Order;
import com.demo.ordermgmnt.model.ProcessInstanceResponse;
import com.demo.ordermgmnt.service.OrderService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/Order")
@CrossOrigin(origins = "*")
public class RestController {

	@Autowired
	OrderService orderService;
	
	@RequestMapping(value = "/testController",method = RequestMethod.POST)
	public String testController(@RequestBody Map<String, Object> orderData) {
		String strOrderType=(String) orderData.get("orderType");
		
		return "Util Controller executed."+ strOrderType;
		
	}
	
	@RequestMapping(value = "/initiateOrder",method = RequestMethod.POST)
	public ProcessInstanceResponse applyHoliday(@RequestBody Order order) {
        return orderService.initOrder(order);
    }
	
	
	
}
