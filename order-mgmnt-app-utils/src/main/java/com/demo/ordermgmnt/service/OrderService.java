package com.demo.ordermgmnt.service;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ordermgmnt.model.Order;
import com.demo.ordermgmnt.model.ProcessInstanceResponse;

@Service
public class OrderService {
	
	 @Autowired
	 RuntimeService runtimeService;

	 public static final String PROCESS_DEFINITION_KEY = "order_mgment_app";
	
	//********************************************************** process endpoints **********************************************************
	    
	 public ProcessInstanceResponse initOrder(Order order) {
	
	        Map<String, Object> variables = new HashMap<String, Object>();
	        variables.put("orderBy", order.getFirst()+" "+order.getLast());
	        variables.put("noOfShipped", order.getNoOfShipped());
	
	        ProcessInstance processInstance =
	                runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
	
	        return new ProcessInstanceResponse(processInstance.getId(), processInstance.isEnded());
	 }

}
