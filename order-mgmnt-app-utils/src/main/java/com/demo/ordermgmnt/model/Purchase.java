package com.demo.ordermgmnt.model;

import java.util.Date;

public class Purchase {

	private int id;
	private Supplier supplier;
	private Product product;
	private int numberReceived;
	private Date purchaseDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getNumberReceived() {
		return numberReceived;
	}
	public void setNumberReceived(int numberReceived) {
		this.numberReceived = numberReceived;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	
}
