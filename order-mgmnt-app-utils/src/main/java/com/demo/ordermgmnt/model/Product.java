package com.demo.ordermgmnt.model;

public class Product {
	
	private String id;
	private String productName;
	private String productLabel;
	private int startingInventory;
	private int inventoryReceived;
	private int inventoryShipped;
	private int inventoryOnHand;
	private int minimumRequired;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductLabel() {
		return productLabel;
	}
	public void setProductLabel(String productLabel) {
		this.productLabel = productLabel;
	}
	public int getStartingInventory() {
		return startingInventory;
	}
	public void setStartingInventory(int startingInventory) {
		this.startingInventory = startingInventory;
	}
	public int getInventoryReceived() {
		return inventoryReceived;
	}
	public void setInventoryReceived(int inventoryReceived) {
		this.inventoryReceived = inventoryReceived;
	}
	public int getInventoryShipped() {
		return inventoryShipped;
	}
	public void setInventoryShipped(int inventoryShipped) {
		this.inventoryShipped = inventoryShipped;
	}
	public int getInventoryOnHand() {
		return inventoryOnHand;
	}
	public void setInventoryOnHand(int inventoryOnHand) {
		this.inventoryOnHand = inventoryOnHand;
	}
	public int getMinimumRequired() {
		return minimumRequired;
	}
	public void setMinimumRequired(int minimumRequired) {
		this.minimumRequired = minimumRequired;
	}
	
	
}
