package com.demo.ordermgmnt.model;

import java.util.Date;

public class Order {

	private int id;
	private String title;
	private String first;
	private String middle;
	private String last;
	private Product product;
	private int noOfShipped;
	private Date orderDate;
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getMiddle() {
		return middle;
	}
	public void setMiddle(String middle) {
		this.middle = middle;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public int getNoOfShipped() {
		return noOfShipped;
	}
	public void setNoOfShipped(int noOfShipped) {
		this.noOfShipped = noOfShipped;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
}