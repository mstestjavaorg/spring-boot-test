package droolsodmrules.demo.service;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import droolsodmrules.demo.model.Order;
import droolsodmrules.demo.model.TaskMaster;

@Service
public class TaskService {
	
	@Autowired
    private KieContainer kContainer;
	
	public TaskMaster getTaskData(String taskName) {
		System.out.println("getTaskData service initiated...");
		
		TaskMaster taskMaster = new TaskMaster();
		taskMaster.setTaskName(taskName);
		
		KieSession kieSession = kContainer.newKieSession();
		kieSession.insert(taskMaster);
		kieSession.fireAllRules();
        kieSession.dispose();
        
        return taskMaster;
	}
	
	public String getPrductListByOrderType(String orderType) {
		System.out.println("getPrductListByOrderType service initiated...");
		Order order = new Order();
		order.setOrderType(orderType);
		
		KieSession kieSession = kContainer.newKieSession();
		kieSession.insert(order);
		kieSession.fireAllRules();
        kieSession.dispose();
        
        return ""+order.getProductName();
	}

}
