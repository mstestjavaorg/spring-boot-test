package droolsodmrules.demo.model;

public class TaskMaster {

	private String taskName;
	private String assigneeGroup;
	private String groupOwner;
	private int tatInDays;
	private int escalationIntervalInDays;
	
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getAssigneeGroup() {
		return assigneeGroup;
	}
	public void setAssigneeGroup(String assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}
	public String getGroupOwner() {
		return groupOwner;
	}
	public void setGroupOwner(String groupOwner) {
		this.groupOwner = groupOwner;
	}
	public int getTatInDays() {
		return tatInDays;
	}
	public void setTatInDays(int tatInDays) {
		this.tatInDays = tatInDays;
	}
	public int getEscalationIntervalInDays() {
		return escalationIntervalInDays;
	}
	public void setEscalationIntervalInDays(int escalationIntervalInDays) {
		this.escalationIntervalInDays = escalationIntervalInDays;
	}
	
	
}