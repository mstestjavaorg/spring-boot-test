package droolsodmrules.demo.controller;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import droolsodmrules.demo.config.KieContainerConfig;
import droolsodmrules.demo.model.TaskMaster;
import droolsodmrules.demo.service.TaskService;

@RestController
@RequestMapping("/DEMO-ODM")
@CrossOrigin(origins = "*")
public class ODMRestController {

	//initiate app-context for KieContainer of task service
	ApplicationContext context = new AnnotationConfigApplicationContext(KieContainerConfig.class);
	TaskService taskService = (TaskService) context.getBean(TaskService.class);
	
	@RequestMapping(value = "/getTaskDetails",method = RequestMethod.POST)
	public TaskMaster getTaskDetails(@RequestBody Map<String, Object> taskInput) {
		
		String strTaskName=(String) taskInput.get("taskName");
		
		return taskService.getTaskData(strTaskName);
	}
	
	@RequestMapping(value = "/getProductListByOrderType",method = RequestMethod.POST)
	public String getProductListByOrderType(@RequestBody Map<String, Object> orderData) {
		String strOrderType=(String) orderData.get("orderType");
		
		return "Rules: Product List for order "
				+ strOrderType + " is " + taskService.getPrductListByOrderType(strOrderType);
		
	}
	
	@RequestMapping(value = "/refreshODMData",method = RequestMethod.POST)
	public String refreshODMData() {
		
		context = new AnnotationConfigApplicationContext(KieContainerConfig.class);
		taskService = (TaskService) context.getBean(TaskService.class);
		
		return "ODM Data Successfully refeshed.";
	}
	
	
}
