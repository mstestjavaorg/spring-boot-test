package droolsodmrules.demo.config;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("droolsodmrules.demo.service")
public class KieContainerConfig {
	
	//public static final String drlFile1 = "com/rules/order_rules.drl";
	public static final String drlFile2 = "com/rules/task_details_rules.xls";

	@Bean
    public KieContainer kieContainer() {
    	System.out.println("kieContainer started...");
        
    	KieServices kieServices = KieServices.Factory.get();

        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
       // kieFileSystem.write(ResourceFactory.newClassPathResource(drlFile1));
        kieFileSystem.write(ResourceFactory.newClassPathResource(drlFile2));
       
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        
        KieModule kieModule = kieBuilder.getKieModule();

        return kieServices.newKieContainer(kieModule.getReleaseId());
    }

}
