FROM java:8

EXPOSE 9081

ADD drools-odm-rules-poc/target/docker-odm-demo.jar docker-odm-demo.jar

ENTRYPOINT ["java","-jar","docker-odm-demo.jar"]
